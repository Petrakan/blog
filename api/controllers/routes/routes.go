package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/Petrakan/blog/api/controllers/handlers"
)

func SetupRoutes(app *fiber.App) {

	//Тестовый роут для homepage
	app.Get("/", handlers.TestHomepage)

	postRoutes(app.Group("/post"))

}

func postRoutes(route fiber.Router) {
	route.Get("/", handlers.GetAllPosts)      // Запрос всего списка постов
	route.Get("/:id", handlers.GetPost)       // Запрос отдельного поста по id
	route.Post("/", handlers.CreatePost)      // Создание поста
	route.Put("/:id", handlers.UpdatePost)    // Обновление поста
	route.Delete("/:id", handlers.DeletePost) // Удаление поста
}
