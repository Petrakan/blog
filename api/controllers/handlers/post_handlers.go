package handlers

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/Petrakan/blog/api/database"
)

//Описываем структуру ответа
type Response struct {
	Message string      `json:"message"`
	Success bool        `json:"success"`
	Data    interface{} `json:"data"` // Наверное не следует использовать здесь пустой интерфейс. TODO: Разобраться с этим позже.
}

var response Response

//Запрос всех постов
func GetAllPosts(c *fiber.Ctx) error {
	db := database.DBConn
	posts := new([]database.Post)
	db.Order("ID desc").Find(&posts)
	if len(*posts) == 0 {
		response.Message = "У вас пока нет постов"
		response.Success = false
		response.Data = nil
		return c.Status(fiber.StatusNotFound).JSON(response)
	}
	response.Message = "All ok"
	response.Success = true
	response.Data = &posts
	return c.Status(fiber.StatusOK).JSON(response)
}

// Запрос отдельного поста по ID
func GetPost(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DBConn
	var post database.Post
	db.Find(&post, id)
	if post.Title == "" {
		response.Message = "Не удалось найти Пост с указанным ID"
		response.Success = true
		response.Data = nil
		return c.Status(fiber.StatusNotFound).JSON(response)
	}
	response.Message = "Пост успешно найден"
	response.Success = true
	response.Data = post
	return c.Status(fiber.StatusOK).JSON(response)
}

//Создание поста
func CreatePost(c *fiber.Ctx) error {
	db := database.DBConn
	post := new(database.Post)
	err := c.BodyParser(post)
	if err != nil {
		response.Message = "Не удалось создать заметку. Неверные входные данные"
		response.Success = false
		response.Data = nil
		log.Fatal(err)
		return c.Status(500).JSON(response)
	}
	db.Create(&post)
	response.Message = "Заметка успешно создана"
	response.Success = true
	response.Data = *post
	return c.Status(fiber.StatusCreated).JSON(response)
}

//Изменеие существующего поста
func UpdatePost(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DBConn
	var oldPost database.Post
	db.First(&oldPost, id)
	if oldPost.Title == "" {
		response.Message = "Не удалось найти заметку с указанным ID"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}

	//Описание структуры получаемых данных
	type Request struct {
		Title   *string  `json:"title"`
		Content *string  `json:"content"`
		Rating  *float64 `json:"rating"`
	}

	var newPost Request
	if err := c.BodyParser(&newPost); err != nil {
		response.Message = "Не удалось создать заметку. Неверные входные данные"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}

	if newPost.Title != nil && newPost.Content != nil {
		oldPost.Title = *newPost.Title
		oldPost.Content = *newPost.Content
		oldPost.Rating = *newPost.Rating
	}
	db.Save(&oldPost)
	response.Message = "Заметка успешно отредактирована"
	response.Success = true
	response.Data = oldPost
	return c.Status(fiber.StatusCreated).JSON(response)
}

//Удаление постов
func DeletePost(c *fiber.Ctx) error {
	id := c.Params("id")
	db := database.DBConn

	var post database.Post
	db.First(&post, id)

	if post.Title == "" {
		response.Message = "Не удалось найти пост с указанным ID"
		response.Success = false
		response.Data = nil
		return c.Status(500).JSON(response)
	}

	db.Delete(&post)

	response.Message = "Заметка успешно удалена"
	response.Success = true
	response.Data = nil
	return c.Status(fiber.StatusOK).JSON(response)

}
