package handlers

import "github.com/gofiber/fiber/v2"

func TestHomepage(c *fiber.Ctx) error {
	return c.SendString("Hello from fiber, this is test homepage")
}
