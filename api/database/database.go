package database

import (
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DBConn *gorm.DB

//Подключение базы данных
func InitDatabase() {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=require TimeZone=Europe/Moscow", os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"), os.Getenv("DB_PORT"))

	var err error
	DBConn, err = gorm.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("Ошибка подключения базы данных: %v", err)
	}
	fmt.Println("Соединение с базой данных установленно")
}
