package database

import "github.com/jinzhu/gorm"

type Post struct {
	gorm.Model
	Title   string  `json:"title"`
	Content string  `json:"content"`
	Rating  float64 `json:"rating"` //Что то не так с рейтингом, в базу могу записать только строки. TODO: Разобраться с базой!
}
