package main

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/Petrakan/blog/api/controllers/routes"
	"gitlab.com/Petrakan/blog/api/database"
)

func main() {

	app := fiber.New()
	//Добавляем чтение из .env
	// if err := godotenv.Load(); err != nil {
	// 	log.Fatalf("Ошибка чтения .env %v", err)
	// } else {
	// 	fmt.Println("Переменные из .env получены.")
	// }

	// Инициализируем базу данных
	database.InitDatabase()
	defer database.DBConn.Close()
	// Автомиграции
	database.DBConn.AutoMigrate(&database.Post{})

	//Подключаем middleware
	//Logger
	app.Use(logger.New())
	//Поддержка cors для подключения клиента
	app.Use(cors.New())

	//Подключение routes
	routes.SetupRoutes(app)

	// Настройки порта
	address := ":" + os.Getenv("PORT")
	if err := app.Listen(address); err != nil {
		log.Fatalf("Ошибка настроеу порта: %v", err)
	}
}
