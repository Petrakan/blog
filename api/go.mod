module gitlab.com/Petrakan/blog/api

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.16.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
)
