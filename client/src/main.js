import { createApp } from 'vue'
import App from './App.vue'
import { store } from '@/vuex/store'
import { router } from './router'
import * as Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library} from '@fortawesome/fontawesome-svg-core'
// import { fas } from '@fortawesome/free-solid-svg-icons'
import { faGithub } from '@fortawesome/free-brands-svg-icons'
import '@/assets/TailwindCSS.css'

const app = createApp(App)


library.add(faGithub)
app.component('fa', FontAwesomeIcon)

app.use(router)
app.use(store)
app.use(VueAxios, axios)

app.mount('#app')
