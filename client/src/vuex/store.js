import { createStore } from 'vuex'

import axios from 'axios'

export const store = createStore({
  state() {
    return {
      posts: [],
    }
  },
  mutations: {
    GET_POSTS: (state, posts) => {
      state.posts = posts.data.data
    },
  },
  actions: {
    GET_POSTSLIST({ commit }) {
      axios.get('https://petrakan-blog.herokuapp.com/post')
        .then((posts) => {
          commit('GET_POSTS', posts)
        })
  .catch((error) => {
          console.log(error)
        })
    },
  },
  getters: {
    POSTS(state) {
      return state.posts;
    },
  }
})
